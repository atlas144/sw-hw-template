# Template

![SW Version](https://img.shields.io/badge/dynamic/json?color=informational&label=SW+version&query=%24.general.version.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)
![PCB Version](https://img.shields.io/badge/dynamic/json?color=informational&label=PCB+version&query=%24.general.version.pcb&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)
![CAD Version](https://img.shields.io/badge/dynamic/json?color=informational&label=CAD+version&query=%24.general.version.cad&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)
[![SW License](https://img.shields.io/badge/dynamic/json?color=green&label=SW+license&query=%24.general.license.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/SW_LICENSE)
[![HW License](https://img.shields.io/badge/dynamic/json?color=green&label=HW+license&query=%24.general.license.hw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/HW_LICENSE)
![Language](https://img.shields.io/badge/dynamic/json?color=red&label=language&query=%24.general.language&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2F-----REPO-----%2Fraw%2Fbranch%2Fmain%2Fshields.json)

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## Description



### Working principle



## Current state

The project is currently at a very early stage of development, so it is likely that most things are not working.

## Using



## Dependencies

- [lib](link) \[1.x.x\] - description

## Contributing

Contributions are welcome!

## License

The source code is licensed under the [MIT](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/SW_LICENSE) license.

The hardware files are licensed under the [CERN Open Hardware License Version 2 - Strongly Reciprocal](https://codeberg.org/atlas144/-----REPO-----/src/branch/main/HW_LICENSE).
